const path = require('path')
const os = require('os')
//destructuring pulling out what we need
const { app, BrowserWindow, Menu, ipcMain, shell } = require('electron')
const imagemin = require('imagemin')
const imageminMozjpeg = require('imagemin-mozjpeg')
const imageminPngquant = require('imagemin-pngquant')
const slash = require('slash')
const log = require('electron-log')


//Set env
// process.env.NODE_ENV = 'development'
process.env.NODE_ENV = 'production'

const isDEV = process.env.NODE_ENV !== 'production' ? true : false
const isMac = process.platform === "darwin" ? true : false
console.log(process.platform)

let mainWindow
let aboutWindow

function createMainWindow() {
    mainWindow = new BrowserWindow({
        title: 'imageShrink',
        width: isDEV ? 1000 : 500,
        height: 600,
        // icon: './assets/icons/Icon_256x256.png',
        resizable: isDEV ? true : false,
        backgroundColor: 'white',
        webPreferences: {
            nodeIntegration: true
        }
    })

    if (isDEV) {
        mainWindow.webContents.openDevTools()
    }
    // mainWindow.loadURL(`file://${ __dirname }/app/index.html`)
    mainWindow.loadFile('./app/index.html')
}

function createAboutWindow() {
    aboutWindow = new BrowserWindow({
        title: 'About imageShrink',
        width: 300,
        height: 300,
        // icon: './assets/icons/Icon_256x256.png',
        resizable: false,
        backgroundColor: 'white'
    })

    // mainWindow.loadURL(`file://${ __dirname }/app/index.html`)
    aboutWindow.loadFile('./app/about.html')
}

app.on('ready', () => {
    createMainWindow()

    const mainMenu = Menu.buildFromTemplate(menu)
    Menu.setApplicationMenu(mainMenu)
    mainWindow.on('closed', () => (mainWindow = null))

})

const menu = [
    ...(isMac
        ? [
            {
                label: app.name,
                submenu: [
                    {
                        label: 'About',
                        click: createAboutWindow,
                    }
                ]

            }] : []),

    {
        role: 'fileMenu'
    },
    ...(!isMac ? [
        {
            label: 'Help',
            submenu: [
                {
                    label: 'About',
                    click: createAboutWindow()
                }
            ]
        }
    ] : []),
    ...(isDEV ? [
        {
            label: 'Developer',
            submenu: [
                { role: 'reload' },
                { role: 'forcereload' },
                { type: 'separator' },
                { role: 'toggledevtools' }
            ]
        }
    ] : [])
]

ipcMain.on('image:minimize', (e, options) => {
    options.dest = path.join(os.homedir(), 'imageshrink')
    shrinkImage(options)
    console.log(options)
})

async function shrinkImage({ imgPath, quality, dest }) {
    try {
        const pngQuality = quality / 100
        const files = await imagemin([slash(imgPath)], {
            destination: dest,
            plugins: [
                imageminMozjpeg({ quality }),
                imageminPngquant({
                    quality: [pngQuality, pngQuality]
                })
            ]
        })

        log.info(files)

        shell.openPath(dest)

        mainWindow.webContents.send('image:done')

    } catch (err) {

        log.error(err)
    }
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (!isMac) {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createMainWindow()
    }
})